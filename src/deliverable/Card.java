
package deliverable;


public abstract class Card {
    
   private String type;
   private String color;
   private static final String [] cardTypes = {"number","action","wild" };
   private static final String [] colors = {"red", "green", "blue", "yellow","black"};
   
   public Card (int cT, int cC){
       type = cardTypes[cT];
       color = colors[cC];
     //  System.out.println(color);
     
   }

    public String getType() {
        return type;
    }

     public String getColor() {
  
        return color;
    }

     public void print(){
         String out="Type = "+getType()+"   Color = "+getColor();
         System.out.print(out);
     }
   
    
   
   
    
}
