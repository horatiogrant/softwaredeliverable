/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package deliverable;

/**
 *
 * @author grant
 */
public class actionCard extends Card {
    private String action;
    private String [] allActions =  {"skip","reverse","plusTwo"};
    
    public actionCard( int cT, int cC,int number){
          super(cT,cC);
           action = allActions[number];
       }
    
    public String getAction(){
        return action;
    }
    
    @Override 
       public void print(){
           super.print();
           System.out.println(" Action = "+getAction());
       }
}
