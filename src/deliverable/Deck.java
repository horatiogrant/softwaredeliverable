
package deliverable;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author grant
 */
public class Deck {
    Random rand = new Random();
    private static ArrayList <Card> deck = new ArrayList();
   // private Card [] deck = new Card [108];
    
    public void createDeck(){
        int ct=0;
        int cC=0;
        int z;
        
       
        for (cC=0;cC<4;cC++){//create numbered cards
             for (z=0;z<19;z++){//runs 19 times to create card nums
                if (z>9){
                    deck.add(new numberCard(ct, cC, z-9)); //makes sure 1-9 is duplicated
                }else{
                     deck.add(new numberCard(ct, cC, z));    
                }                     
            }    
        }//for loop for numbered cards
     ct++;
         for (z=0;z<3;z++){//create action cards
             for (cC=0;cC<4;cC++){
                 deck.add(new actionCard(ct, cC, z));//action card twice
                 deck.add(new actionCard(ct, cC, z));
             }
             
         }
         
         ct++;
         for (z=0;z<8;z++){
             if (z<4){
                   deck.add(new wildCard(ct, 4, 0));//create switch colour wild card  
             }else{
                 deck.add(new wildCard(ct, 4, 1));//creates plus 4 wild card
             }
         }/*
                for (int x=0;x<76;x++){
               deck.get(x).print();
           }
                  for (int x=76;x<100;x++){
               deck.get(x).print();
           }
                        for (int x=100;x<108;x++){
               deck.get(x).print();
           }
          */       
      
            
  //  }//108 loop
    
}//createDeck Brack
    
    public Card getACard(){//to assign cards
        Card card;
        int n = rand.nextInt(deck.size());
        card =deck.get(n);
        deck.remove(n);
       return card;
        
    }
    
    public ArrayList<Card> getDeck(){
        return deck;
    }
}
