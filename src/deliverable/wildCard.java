/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package deliverable;

/**
 *
 * @author grant
 */

public class wildCard extends Card{
    private String wild;
    private String [] allWild =  {"switchColor","plusFour"};
    public wildCard(int cT, int cC, int number){
        super(cT,cC);
        wild = allWild[number];
    }
    
    public String getWild(){
        return wild;
    }

   @Override 
       public void print(){
           super.print();
           System.out.println(" Wild = "+getWild());
       }
}
