/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package deliverable;

/**
 *
 * @author grant
 */
public class numberCard extends Card{
       private int number ; 
       
       public numberCard( int cT,int cC,int number){
           super(cT,cC);
           this.number = number;
       }
       
       public int getNum(){
           return number;
       }
       
       @Override 
       public void print(){
           super.print();
           System.out.println(" Number = "+getNum());
       }
}
