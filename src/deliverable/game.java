/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package deliverable;

/**
 *
 * @author grant
 */
public class game {
    
    
    public void newGame(User user, Bot bot1, Bot bot2, Bot bot3){
        
         Deck deck = new Deck();
         deck.createDeck();
          System.out.println("Size = "+deck.getDeck().size());
         for (int x=0;x<7;x++){
              user.addCard(deck.getACard());
              bot1.addCard(deck.getACard());
              bot2.addCard(deck.getACard());
              bot3.addCard(deck.getACard());
         }
         System.out.println("--------------------- \nUsers Hand\n ---------------------");
         for (int x=0;x<7;x++){
              user.hand.get(x).print();
         }
          System.out.println("--------------------- \nBot1 Hand\n ---------------------");
         for (int x=0;x<7;x++){
              bot1.hand.get(x).print();
         }
                   System.out.println("--------------------- \nBot2 Hand\n ---------------------");

         for (int x=0;x<7;x++){
              bot2.hand.get(x).print();
         }
                   System.out.println("--------------------- \n Bot3 Hand \n ---------------------");

         for (int x=0;x<7;x++){
              bot3.hand.get(x).print();
         }
         System.out.println("Size = "+deck.getDeck().size());
       
    }
    
}
