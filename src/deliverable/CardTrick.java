/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deliverable;

/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
public class CardTrick {
    
    public static void main(String[] args)
    {
       User user = new User();
       Bot bot1 = new Bot();
        Bot bot2 = new Bot();
         Bot bot3 = new Bot();
     game g = new game();
     g.newGame(user, bot1, bot2, bot3);
    } 
}
